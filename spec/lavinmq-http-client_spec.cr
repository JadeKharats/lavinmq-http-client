require "./spec_helper"
require "json"

describe Lavinmq::HTTPClient do
  client = Lavinmq::HTTPClient.new("http://guest:guest@localhost:15672")

  it "set headers for application/json" do
    Lavinmq::HTTPClient::HEADERS.should eq HTTP::Headers{"Content-Type" => "application/json"}
  end

  it "have an host set by call" do
    ENV.delete("LAVINMQCTL_HOST")
    client_with_host = Lavinmq::HTTPClient.new("http://guest:guest@localhost:15672")
    client_with_host.host.should eq "http://guest:guest@localhost:15672"
  end

  it "have an host set by environment variable" do
    ENV["LAVINMQCTL_HOST"] = "http://guest_env:guest@localhost:15672"
    client_with_env_host = Lavinmq::HTTPClient.new("http://guest:guest@localhost:15672")
    ENV.delete("LAVINMQCTL_HOST")
    client_with_env_host.host.should eq "http://guest_env:guest@localhost:15672"
  end

  it "return the list of users" do
    list_users = client.list_users
    list_users.size.should be >= 1
    list_users[0].name.should eq "guest"
  end

  it "return a user" do
    user = client.get_user("guest")
    user.tags.should eq "administrator"
  end

  it "add user" do
    client.add_user("toto", "titi")
    list_users = client.list_users
    list_users[-1].name.should eq "toto"
  end

  it "delete user" do
    client.delete_user("toto")
    expect_raises(Lavinmq::HTTPException) do
      client.get_user("toto")
    end
  end

  it "set user tags" do
    client.add_user("test_tags", "plop")
    client.set_user_tags("test_tags", ["administrator", "impersonator"])
    user = client.get_user("test_tags")
    user.tags.should eq "administrator,impersonator"
  end

  it "change password for an user" do
    client.add_user("user_change_password", "tata")
    user = client.get_user("user_change_password")
    old_hash = user.password_hash
    client.change_password("user_change_password", "plop")
    user = client.get_user("user_change_password")
    user.password_hash.should_not eq old_hash
  end

  it "return the list of virtual hosts" do
    vhosts = client.list_vhosts
    vhosts[0].name.should eq "/"
  end

  it "add vhost by name" do
    client.add_vhost("test_add_vhost")
    vhosts = client.list_vhosts
    vhosts_names = vhosts.map(&.name)
    vhosts_names.should contain("test_add_vhost")
  end

  it "get vhost by name" do
    client.add_vhost("test_get_vhost")
    vhost = client.get_vhost("test_get_vhost")
    vhost.name.should eq "test_get_vhost"
  end

  it "delete vhost by name" do
    client.add_vhost("test_delete_vhost")
    vhost = client.get_vhost("test_delete_vhost")
    vhost.name.should eq "test_delete_vhost"
    client.delete_vhost("test_delete_vhost")
    expect_raises(Lavinmq::HTTPException) do
      client.get_vhost("test_delete_vhost")
    end
  end

  it "reset a vhost by name without backup" do
    # TODO : Add function to add message in a queue
    client.add_vhost("test_reset_vhost")
    client.reset_vhost("test_reset_vhost")
    vhost = client.get_vhost("test_reset_vhost")
    vhost.messages.should eq 0
  end

  it "reset a vhost by name with backup" do
    client.add_vhost("test_reset_vhost_backup")
    client.reset_vhost("test_reset_vhost_backup", true, "Pokemon")
    vhost = client.get_vhost("test_reset_vhost_backup")
    vhost.messages.should eq 0
  end

  it "reset a vhost by name with backup with backup dir" do
    client.add_vhost("test_reset_vhost_backup")
    client.reset_vhost("test_reset_vhost_backup", true)
    vhost = client.get_vhost("test_reset_vhost_backup")
    vhost.messages.should eq 0
  end
end
