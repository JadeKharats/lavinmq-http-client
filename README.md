# lavinmq-http-client

Client for LavinMQ HTTP API.
Its primary use is to be integrated into the CLI

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     lavinmq-http-client:
       gitlab: JadeKharats/lavinmq-http-client
   ```

2. Run `shards install`

## Usage

```crystal
require "lavinmq-http-client"
```

## Development

Install :
- [guardian](https://github.com/f/guardian#installation)
- Docker et Docker-compose

Launch :
- `docker compose up -d` to have a lavinmq instance ready
- `guardian -c` to execute test when you save `spec` or `src` files

## Contributing

Follow gitlab-flow :

- Create an issue.
- Create merge request from issue.
- Pull the merge request branch.
- Remove WIP markers

## Contributors

- [Jade D. Kharats](https://gitlab.com/JadeKharats) - creator and maintainer
