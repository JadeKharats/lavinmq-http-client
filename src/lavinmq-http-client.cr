require "http/client"
require "json"

class Lavinmq::HTTPClient
  @http : HTTP::Client?
  getter host : String?
  HEADERS = HTTP::Headers{"Content-Type" => "application/json"}

  def initialize(host : String)
    if ENV["LAVINMQCTL_HOST"]?
      @host = ENV["LAVINMQCTL_HOST"]
    else
      @host = host
    end
  end

  private def connect
    if host = @host
      uri = URI.parse(host)
      client = HTTP::Client.new(uri)
      client.basic_auth(uri.user, uri.password) if uri.user
      client
    else
      raise Exception.new("bad host: #{@host}")
    end
  end

  private def http
    @http ||= connect
  end

  private def handle_response(resp, *ok)
    return if ok.includes? resp.status_code
    raise Lavinmq::HTTPException.new(resp.status, resp.status_code, resp.body)
  end

  def list_users
    resp = http.get "/api/users", HEADERS
    handle_response(resp, 200)
    Array(Lavinmq::HTTPUsers).from_json(resp.body)
  end

  def get_user(username : String)
    resp = http.get "/api/users/#{username}", HEADERS
    handle_response(resp, 200)
    Lavinmq::HTTPUsers.from_json(resp.body)
  end

  def add_user(username : String, password : String)
    resp = http.put "/api/users/#{username}", HEADERS, {password: password}.to_json
    handle_response(resp, 201, 204)
  end

  def delete_user(username : String)
    resp = http.delete "/api/users/#{username}", HEADERS
    handle_response(resp, 204)
  end

  def set_user_tags(username : String, tags : Array(String))
    resp = http.put "/api/users/#{username}", HEADERS, {tags: tags.join(",")}.to_json
    handle_response(resp, 204)
  end

  def change_password(username : String, password : String)
    resp = http.put "/api/users/#{username}", HEADERS, {password: password}.to_json
    handle_response(resp, 204)
  end

  def list_vhosts
    resp = http.get "/api/vhosts", HEADERS
    handle_response(resp, 200)
    Array(Lavinmq::HTTPVHost).from_json(resp.body)
  end

  def add_vhost(vhost_name : String)
    resp = http.put "/api/vhosts/#{vhost_name}", HEADERS
    handle_response(resp, 201, 204)
  end

  def get_vhost(vhost_name : String)
    resp = http.get "/api/vhosts/#{vhost_name}", HEADERS
    handle_response(resp, 200)
    Lavinmq::HTTPVHost.from_json(resp.body)
  end

  def delete_vhost(vhost_name : String)
    resp = http.delete "/api/vhosts/#{vhost_name}", HEADERS
    handle_response(resp, 204)
  end

  def reset_vhost(vhost_name : String, backup = false, backup_dir_name = "")
    body = if backup
             dir_name = Time.utc.to_unix.to_s if backup_dir_name.empty?
             {
               "backup":          true,
               "backup_dir_name": dir_name,
             }
           end
    body ||= {} of String => String
    resp = http.post "/api/vhosts/#{vhost_name}/purge_and_close_consumers", HEADERS, body.to_json
    handle_response(resp, 204)
  end
end

class Lavinmq::HTTPException < Exception
  @status : HTTP::Status
  @status_code : Int32
  @body : String

  def initialize(@status, @status_code, @body)
    @message = "#{@status_code} - #{@status} : #{@body}"
  end
end

class Lavinmq::HTTPVHost
  include JSON::Serializable
  property name : String
  property dir : String
  property messages : UInt32
  property messages_unacknowledged : UInt32
  property messages_ready : UInt32
  property message_stats : Lavinmq::MessageStats
end

class Lavinmq::MessageStats
  include JSON::Serializable
  property ack : UInt32
  property confirm : UInt32
  property deliver : UInt32
  property get : UInt32
  property get_no_ack : UInt32
  property publish : UInt32
  property redeliver : UInt32
  property return_unroutable : UInt32
end

class Lavinmq::HTTPUsers
  include JSON::Serializable
  property name : String
  property password_hash : String
  property hashing_algorithm : String
  property tags : String
end
